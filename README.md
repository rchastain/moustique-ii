
# Moustique II

## Overview

Simple chess engine using [CECP](http://hgm.nubati.net/CECP.html) also known as XBoard or WinBoard protocol.

The main program is the literal rewriting in Pascal of the [WinBoard protocol driver example](http://www.open-aurec.com/wbforum/viewtopic.php?f=24&t=51739) by Harm Geert Muller.

The core of the engine is derived from a [didactic Turbo Pascal program by Jürgen Schlottke](https://gitlab.com/rchastain/moustique/-/blob/master/original/schach.txt).

## Logo

![alt text](https://gitlab.com/rchastain/moustique-ii/-/raw/master/logo/Farman%20F455%20Moustique.bmp)
