
(* --------------------------------------------------------- *)
(*                                                           *)
(*  Example of a WinBoard-protocol driver, by H. G. Muller.  *)
(*                                                           *)
(*  Pascal version by R. Chastain.                           *)
(*                                                           *)
(* --------------------------------------------------------- *)

uses
{$IFDEF UNIX}
  CThreads, CWString,
{$ENDIF}
  SysUtils, Classes, Player, PlayerTypes, Log;

const
// four different constants, with values for WHITE and BLACK that suit your engine
  WHITE   = 1;
  BLACK   = 2;
  NONE    = 0;
  ANALYZE = 3;

// some value that cannot occur as a valid move
  INVALID = 666;

// some parameter of your engine
  MAXMOVES = 500;  (* maximum game length  *)
  MAXPLY   = 60;   (* maximum search depth *)

  OFF = 0;
  ON_ = 1;

  DEFAULT_FEN = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

type
  TMOVE = integer;                         // in this example moves are encoded as an int      

var
  moveNr: integer = 0;                     // part of game state; incremented by MakeMove
  gameMove: array[0..MAXMOVES-1] of TMOVE; // holds the game history

// Some routines your engine should have to do the various essential things
function MakeMove(const stm: integer; const move_: TMOVE): integer; forward; // performs move, and returns new side to move
procedure UnMake(const move_: TMOVE); forward;                               // unmakes the move;
function SetUp(const fen: string): integer; forward;                         // sets up the position from the given FEN, and returns the new side to move
procedure SetMemorySize(const n: integer); forward;                          // if n is different from last time, resize all tables to make memory usage below n MB
function MoveToText(const move_: TMOVE): string; forward;                    // converts the move from your internal format to text like e2e2, e1g1, a7a8q.
function ParseMove(const moveText: string): integer; forward;                // converts a long-algebraic text move to your internal move format
function SearchBestMove(const stm, timeLeft, mps, timeControl, inc_, timePerMove: integer; var move_, ponderMove: TMOVE): integer; forward;
procedure PonderUntilInput(const stm: integer); forward;                     // Search current position for stm, deepening forever until there is input.

// Some global variables that control your engine's behavior
var
  ponder,
  randomize_,
  postThinking,
  resign,                  // engine-defined option
  contemptFactor: integer; // likewise

procedure Send(const AMessage: string);
begin
  LogLn(Format('< %s', [AMessage]));
  WriteLn(AMessage);
  Flush(output);
end;

function TakeBack(const n: integer): integer; // reset the game and then replay it to the desired point
var
  last, stm, nr: integer;
begin
  stm := SetUp('');
  last := moveNr - n;
  if last < 0 then last := 0;
  for nr := 0 to Pred(moveNr) do stm := MakeMove(stm, gameMove[nr]);
  result := stm;
end;

procedure PrintResult(const stm, score: integer);
begin
  if score = 0 then
    Send('1/2-1/2')
  else if (score > 0) and (stm = WHITE) or (score < 0) and (stm = BLACK) then
    Send('1-0')
  else
    Send('0-1');
end;

function main: integer;
var
  stm: integer;                                 // side to move
  engineSide: integer = NONE;                   // side played by engine
  timeLeft: integer;
  mps, timeControl, inc_, timePerMove: integer; // time-control parameters, to be used by Search
  maxDepth: integer;                            // used by search
  move_, ponderMove: integer;
  inBuf: string;
  command: string;
  score: integer;
  min: integer;
  sec: integer = 0;
  //newStm: integer;
  nping: integer;
label
  noPonder;
begin
  LogLn(ParamStr(0), TRUE);
  while {not EOF}TRUE do // infinite loop
  begin
    LogLn(Format('stm = %d engineSide = %d', [stm, engineSide]));
    Flush(output);                   // make sure everything is printed before we do something that might take time
    if stm = engineSide then         // if it is the engine's turn to move, set it thinking, and let it move
    begin
      score := SearchBestMove(stm, timeLeft, mps, timeControl, inc_, timePerMove, move_, ponderMove);
      if move_ = INVALID then        // game apparently ended
      begin
        engineSide := NONE;          // so stop playing
        PrintResult(stm, score);
      end else
      begin
        stm := MakeMove(stm, move_); // assumes MakeMove returns new side to moves
        gameMove[moveNr] := move_;   // remember game
        Inc(moveNr);
        Send(Format('move %s', [MoveToText(move_)]));
      end;
    end;
    (*
    Flush(output); // make sure everything is printed before we do something that might take time
    // now it is not our turn (anymore)
    if engineSide = ANALYZE then       // in analysis, we always ponder the position
        PonderUntilInput(stm)
    else
    if (engineSide <> NONE) and (ponder = ON_) and (moveNr <> 0) then // ponder while waiting for input
    begin
      if ponderMove = INVALID then      // if we have no move to ponder on, ponder the position
        PonderUntilInput(stm)
      else
      begin
        newStm := MakeMove(stm, ponderMove);
        PonderUntilInput(newStm);
        UnMake(ponderMove);
      end;
    end;
    *)
    noPonder:
    ReadLn(inBuf);
    LogLn(Format('> %s', [inBuf]));
    SScanf(inBuf, '%s', [@command]);
    
    if command = 'quit'    then Break;
    if command = 'force'   then begin engineSide := NONE;    Continue; end;
    if command = 'analyze' then begin engineSide := ANALYZE; Continue; end;
    if command = 'exit'    then begin engineSide := NONE;    Continue; end;
    if command = 'otim'    then goto noPonder; // do not start pondering after receiving time commands, as move will follow immediately
    if command = 'time'    then begin SScanf(inBuf, 'time %d', @timeLeft); goto noPonder; end;
    if command = 'level'   then
    begin
      if (SScanf(inBuf, 'level %d %d %d', [@mps, @min, @inc_]) = 3) // if this does not work, it must be min:sec format
      or (SScanf(inBuf, 'level %d %d:%d %d', [@mps, @min, @sec, @inc_]) = 4) then
        ;
      timeControl := 60 * min + sec;
      timePerMove := -1;
      Continue;
    end;
    if command = 'protover' then
    begin
      Send('feature ping=1 setboard=1 usermove=1 memory=1 sigint=0 sigterm=0 myname="Moustique II 0.2"');
      Send('feature done=1');
      Continue;
    end;
    if command = 'option' then // setting of engine-define option; find out which
    begin
      (*
      if(sscanf(inBuf+7, "Resign=%d",   &resign)         == 1) continue;
      if(sscanf(inBuf+7, "Contempt=%d", &contemptFactor) == 1) continue;
      continue;
      *)
    end;
    if command = 'sd'       then begin SScanf(inBuf, 'sd %d', [@maxDepth]); Continue; end;
    if command = 'st'       then begin SScanf(inBuf, 'st %d', [@timePerMove]); Continue; end;
    if command = 'memory'   then begin SetMemorySize(StrToIntDef(Copy(inBuf, 8), 0)); Continue; end;
    if command = 'ping'     then begin SScanf(inBuf, 'ping %d', [@nping]); Send(Format('pong %d', [nping])); Continue; end;
    if command = 'new'      then begin engineSide := BLACK; stm := Setup(DEFAULT_FEN); maxDepth := MAXPLY; randomize_ := OFF; Continue; end;
    if command = 'setboard' then begin  engineSide := NONE;  stm := Setup(Copy(inBuf, 10)); Continue; end;
    if command = 'easy'     then begin ponder := OFF; Continue; end;
    if command = 'hard'     then begin ponder := ON_; Continue; end;
    if command = 'undo'     then begin stm := TakeBack(1); Continue; end;
    if command = 'remove'   then begin stm := TakeBack(2); Continue; end;
    if command = 'go'       then begin engineSide := stm; Continue; end;
    if command = 'post'     then begin postThinking := ON_; Continue; end;
    if command = 'nopost'   then begin postThinking := OFF; Continue; end;
    if command = 'random'   then begin randomize_ := ON_; Continue; end;
    if command = 'hint'     then begin if ponderMove <> INVALID then Send(Format('Hint: %s', [MoveToText(ponderMove)])); Continue; end;
    if command = 'book'     then Continue;
    // ignored commands:
    if command = 'xboard'   then Continue;
    if command = 'computer' then Continue;
    if command = 'name'     then Continue;
    if command = 'ics'      then Continue;
    if command = 'accepted' then Continue;
    if command = 'rejected' then Continue;
    if command = 'variant'  then Continue;
    if command = ''         then Continue;
    if command = 'usermove' then
    begin
      move_ := ParseMove(Copy(inBuf, 10));
      if move_ = INVALID then Send('Illegal move')
      else
      begin
        stm := MakeMove(stm, move_);
        ponderMove := INVALID;
        Inc(moveNr);
        gameMove[moveNr] := move_;  // remember game
      end;
      Continue;
    end;
    if command = '?' then
    begin
      Send('move a1a1');
      Continue;
    end;
    LogLn(Format('Unknown command: "%s"', [inBuf]));
  end;
  result := 0;
end;

var
  LPlayer: TChessPlayer;
  
function MakeMove(const stm: integer; const move_: TMOVE): integer;
begin
  LogLn(Format('MakeMove(%d, %d)', [stm, move_]));
  if LPlayer.PlayMove(move_) then
  begin
    if stm = WHITE then
      result := BLACK
    else
      result := WHITE;
  end else
    LogLn(Format('Move estimated illegal by the artificial chess player: %d', [move_]));
  LogLn(Format('MakeMove = %d', [result]));
end;

procedure UnMake(const move_: TMOVE);
begin
end;

function SetUp(const fen: string): integer;
begin
  LogLn(Format('SetUp(%s)', [fen]));
  LPlayer.SetPosition(fen);
  result := WHITE;
  LogLn(Format('SetUp = %d', [result]));
end;

procedure SetMemorySize(const n: integer);
begin
end;

function MoveToText(const move_: TMOVE): string;
begin
  //Log(Format('MoveToText(%d)', [move_]));
  result := PlayerTypes.MoveToStr(move_);
end;

function ParseMove(const moveText: string): integer;
begin
  //Log(Format('ParseMove(%s)', [moveText]));
  result := PlayerTypes.MoveToInt(moveText);
end;

function SearchBestMove(const stm, timeLeft, mps, timeControl, inc_, timePerMove: integer; var move_, ponderMove: TMOVE): integer;
begin
  //Log(Format('SearchBestMove(...)', ));
  move_ := LPlayer.BestMove;
  result := 0;
end;

procedure PonderUntilInput(const stm: integer);
begin
end;

begin
  LPlayer := TChessPlayer.Create;
  ExitCode := main;
  LPlayer.Free;
end.
