
{ Autor:   Jürgen Schlottke, Schönaich-C.-Str. 46, D-W 2200 Elmshorn
           Tel. 04121/63109
  Zweck  : Demonstration der Schachprogrammierung unter Turbo-Pascal
  Datum  : irgendwann 1991, als PD freigegeben am 18.01.93
  Version: ohne Versionsnummer
}

unit Player;

interface

uses
  PlayerCore;

type
  TExitCode = (ecSuccess, ecCheck, ecCheckmate, ecStalemate, ecError);
  
  TChessPlayer = class
  strict private
    FIniPos,
    FCurPos: TChessPosition;
    FMoveCount: integer;
    FList: TMoveList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetPosition(const APos: string);
    function PlayMove(const AMove: integer): boolean;
    function BestMoveIndex(const ABestValue: integer): integer;
    function BestMove(out ACode: TExitCode): integer; overload;
    function BestMove: integer; overload;
    function BoardAsText(const APretty: boolean = TRUE): string;
    function FEN: string;
  end;

implementation

uses
  Classes, SysUtils, TypInfo, ChessPlayerTypes, Log;

const
  CStartPos = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';

constructor TChessPlayer.Create;
begin
  inherited Create;
  FIniPos := TChessPosition.Create(CStartPos);
  FCurPos := TChessPosition.Create;
end;

destructor TChessPlayer.Destroy;
begin
  FCurPos.Free;
  FIniPos.Free;
  inherited Destroy;
end;

procedure TChessPlayer.SetPosition(const APos: string);

  function GetMoveCount(const APos: string): integer;
  var
    LFields: TStringList;
  begin
    LFields := TStringList.Create;
    ExtractStrings([' '], [], pchar(APos), LFields);
    if LFields.Count = 6 then
      result := 2 * Pred(StrToInt(LFields[5])) + Ord(LFields[1] = 'b')
    else
      result := 0;
    LFields.Free;
  end;

begin
  LogLn(Format('TChessPlayer.SetPosition(%s)', [APos]));
  FCurPos.SetPosition(APos);
  FMoveCount := GetMoveCount(APos);
end;

function TChessPlayer.PlayMove(const AMove: integer): boolean;
var
  LPos: TChessPosition;
  LMove, LPr, LFr, LTo: integer;
begin
  LogLn(Format('TChessPlayer.PlayMove(%d) [FCurPos.Active = %d]', [AMove, FCurPos.Active]));
  LPr   := AMove div 10000;
  LMove := AMove mod 10000;
  LFr := LMove div 100;
  LTo := LMove mod 100;
  FCurPos.GenerateMoves;
  result := FCurPos.IsLegal(LMove);
  if result then
  begin
    LPos := TChessPosition.Create;
    LPos.SetPosition(FCurPos);
    LPos.MovePiece(LFr, LTo, LPr);
    LPos.Active := CBlack * LPos.Active;
    if LPos.Check then
      result := FALSE
    else
    begin
      FCurPos.MovePiece(LFr, LTo, LPr);
      Inc(FMoveCount);
    end;
    LPos.Free;
  end;
  LogLn(Format('PlayMove = %s [FCurPos.Active = %d]', [BoolToStr(result, TRUE), FCurPos.Active]));
end;

function TChessPlayer.BestMoveIndex(const ABestValue: integer): integer;
var
  LPos: TChessPosition;
  LMaxValue: integer;
  LCount: integer;
  i, j: integer;
begin
  LPos := TChessPosition.Create;

  with FCurPos do
  begin
    LCount := 0;
    for i := 1 to MoveCount do
      if MoveList[i].FVal = ABestValue then
      begin
        Inc(LCount);
        FList[LCount].FFr := MoveList[i].FFr;
        FList[LCount].FTo := MoveList[i].FTo;
        FList[LCount].FVal := 0
      end;
  end;

  LMaxValue := Low(integer);
  result := 0;
  
  for i := 1 to LCount do
  begin
    LPos.SetPosition(FCurPos);

    with LPos do
    begin
      if Board[FList[i].FFr] = FIniPos.Board[FList[i].FFr] then
      begin
        Inc(FList[i].FVal, 5);
        if Board[FList[i].FFr] * Active = CPawn then
          Inc(FList[i].FVal, 2);
      end;

      if Board[FList[i].FFr] * Active = CKing then
        if Abs(FList[i].FTo - FList[i].FFr) = 20 then
          Inc(FList[i].FVal, 20)
        else
          Dec(FList[i].FVal, 10);

      if (FMoveCount < 32) and (Board[FList[i].FFr] * Active in [CPawn, CBishop, CKnight]) then
        Inc(FList[i].FVal, 20);

      if (FList[i].FFr div 10 = 1)
      or (FList[i].FFr div 10 = 8)
      or (FList[i].FFr mod 10 = 1)
      or (FList[i].FFr mod 10 = 8) then
        Inc(FList[i].FVal, 2);

      if (FList[i].FTo div 10 = 1)
      or (FList[i].FTo div 10 = 8)
      or (FList[i].FTo mod 10 = 1)
      or (FList[i].FTo mod 10 = 8) then
        Dec(FList[i].FVal, 2);
    end;

    LPos.SetPosition(FCurPos);
    LPos.MovePiece(FList[i].FFr, FList[i].FTo, CQueen);

    if LPos.Board[FList[i].FTo] = FIniPos.Board[FList[i].FTo] then
      Dec(FList[i].FVal, 10);

    LPos.Active := CBlack * LPos.Active;
    LPos.GenerateSimpleMoves;

    with LPos do
      for j := 1 to MoveCount do
      begin
        Inc(FList[i].FVal);
        if Board[MoveList[j].FTo] <> CNil then
          Inc(FList[i].FVal);
      end;

    LPos.Active := CBlack * LPos.Active;
    LPos.GenerateSimpleMoves;

    with LPos do
      for j := 1 to MoveCount do
      begin
        Dec(FList[i].FVal);
        if Board[MoveList[j].FTo] <> CNil then
          Dec(FList[i].FVal);
      end;
    if FList[i].FVal >= LMaxValue then
    begin
      LMaxValue := FList[i].FVal;
      result := i;
    end;
  end;
  LPos.Free;
end;

function TChessPlayer.BestMove(out ACode: TExitCode): integer;
const
  CExitCode: array[boolean, boolean] of TExitCode = ((ecStalemate, ecCheckmate), (ecSuccess, ecCheck));
var
  i: integer;
  LCheckBefore, LPr: boolean;
  LPos: TChessPosition;
begin
  result := 0;
  i := FCurPos.BestEval(FCurPos.Active, 1, 32000);
  i := BestMoveIndex(i);
  if i > 0 then
  begin
    LPos := TChessPosition.Create;
    LPos.SetPosition(FCurPos);
    LCheckBefore := LPos.Check;
    with FList[i] do
      LPr := LPos.MovePiece(FFr, FTo, CQueen);
    LPos.Active := CBlack * LPos.Active;
    if LPos.Check then { Illegal move }
      ACode := CExitCode[FALSE, LCheckBefore]
    else
    begin
      with FList[i] do
        result := 100 * FFr + FTo;
      if LPr then
        result := result + 10000 * CQueen;
      LPos.Active := CBlack * LPos.Active;
      ACode := CExitCode[TRUE, LPos.Check];
    end;
    LPos.Free;
  end else
    ACode := ecError;
  if not (ACode in [ecSuccess, ecCheck]) then
    LogLn(Format('bestmove function exit code %d', [Ord(ACode)]));
end;

function TChessPlayer.BestMove: integer;
var
  LCode: TExitCode;
begin
  LogLn('TChessPlayer.BestMove');
  LogLn(LineEnding + BoardAsText(TRUE));
  result := BestMove(LCode);
end;

function TChessPlayer.BoardAsText(const APretty: boolean): string;
begin
  result := FCurPos.BoardAsText(APretty);
end;

function TChessPlayer.FEN: string;
begin
  result := FCurPos.FEN;
end;

end.
