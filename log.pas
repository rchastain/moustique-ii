
unit Log;

interface

procedure LogLn(const AText: string; const ARewrite: boolean = FALSE);

implementation

uses
  SysUtils;

procedure LogLn(const AText: string; const ARewrite: boolean);
var
  LFile: text;
  LName: string;
begin
  LName := ChangeFileExt(ParamStr(0), '.log');
  Assign(LFile, LName);
  if ARewrite or not FileExists(LName) then
    Rewrite(LFile)
  else
    Append(LFile);
  WriteLn(LFile, Format('%s %s', [TimeToStr(Time), AText]));
  Close(LFile);
end;

end.
