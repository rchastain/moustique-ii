
{ Autor:   Jürgen Schlottke, Schönaich-C.-Str. 46, D-W 2200 Elmshorn
           Tel. 04121/63109
  Zweck  : Demonstration der Schachprogrammierung unter Turbo-Pascal
  Datum  : irgendwann 1991, als PD freigegeben am 18.01.93
  Version: ohne Versionsnummer
}

unit PlayerTypes;

interface

type
  TChessboard = array[-10..109] of shortint;

const
  CNil    =   0;
  CWhite  =   1;
  CBlack  =  -1;
  CPawn   =   2;
  CBishop =   6;
  CKnight =   7;
  CRook   =  10;
  CQueen  =  19;
  CKing   = 126;
  CBorder = 127;
  
function ColToInt(const ACol: char): integer; 
function RowToInt(const ARow: char): integer;
function DigitToRow(const ADigit: char): char;
function MoveToInt(const AMove: string): integer;
function MoveToStr(const AMove: integer): string;
function PieceToInt(const APiece: char): shortint;
function PieceToChar(const APiece: shortint): char;

implementation

uses
  SysUtils;

function ColToInt(const ACol: char): integer;
begin
  result := Ord(ACol) - Ord('a') + 1;
end;

function RowToInt(const ARow: char): integer;
begin
  result := Ord(ARow) - Ord('1') + 1;
end;

function DigitToRow(const ADigit: char): char;
begin
  result := Chr(Ord(ADigit) - Ord('1') + Ord('a'));
end;

function RowToDigit(const ARow: char): char;
begin
  result := Chr(Ord(ARow) - Ord('a') + Ord('1'));
end;

function MoveToStr(const AMove: integer): string;
var
  LPromotion: integer;
begin
  result := IntToStr(AMove mod 10000);
  result[1] := DigitToRow(result[1]);
  result[3] := DigitToRow(result[3]);
  LPromotion := AMove div 10000;
  if LPromotion <> 0 then
    result := Concat(result, PieceToChar(LPromotion));
end;

function MoveToInt(const AMove: string): integer;
var
  s: string;
begin
  s := Copy(AMove, 1, 4);
  s[1] := RowToDigit(s[1]);
  s[3] := RowToDigit(s[3]);
  result := StrToInt(s);
  if Length(AMove) > 4 then
    case AMove[5] of
      'b': Inc(result, 10000 * CBishop);
      'n': Inc(result, 10000 * CKnight);
      'r': Inc(result, 10000 * CRook);
      'q': Inc(result, 10000 * CQueen);
      else WriteLn(ErrOutput, 'Unexpected character: ' + AMove[5]);
    end;
end;

function PieceToInt(const APiece: char): shortint;
begin
  case APiece of
    'P': result := CWhite * CPawn;
    'N': result := CWhite * CKnight;
    'B': result := CWhite * CBishop;
    'R': result := CWhite * CRook;
    'Q': result := CWhite * CQueen;
    'K': result := CWhite * CKing;
    'p': result := CBlack * CPawn;
    'n': result := CBlack * CKnight;
    'b': result := CBlack * CBishop;
    'r': result := CBlack * CRook;
    'q': result := CBlack * CQueen;
    'k': result := CBlack * CKing;
    else result := CNil;
  end;
end;

function PieceToChar(const APiece: shortint): char;
begin
  case APiece of
    CWhite * CPawn  : result := 'P';
    CWhite * CKnight: result := 'N';
    CWhite * CBishop: result := 'B';
    CWhite * CRook  : result := 'R';
    CWhite * CQueen : result := 'Q';
    CWhite * CKing  : result := 'K';
    CBlack * CPawn  : result := 'p';
    CBlack * CKnight: result := 'n';
    CBlack * CBishop: result := 'b';
    CBlack * CRook  : result := 'r';
    CBlack * CQueen : result := 'q';
    CBlack * CKing  : result := 'k';
    CNil            : result := '.';
    CBorder         : result := '#'
    else              result := '?';
  end;
end;

end.
